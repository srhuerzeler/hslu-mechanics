"""Build recipes for the conditor python package."""

import conditor.recipe
import conditor.py3pkg.recipe
import conditor.py3pkg.build

DEFAULT_RECIPE = 'HSLUMechanics'

class HSLUMechanics (conditor.py3pkg.recipe.Package) :
    """Default hslu-mechanics python package build."""
    NAME = 'hslumechanics-core'
    def configure(self, build, flavour) :
        package_root = self.project.path.joinpath('./src/pkg_hsmech').resolve()
        package = conditor.py3pkg.build.Package(build)
        build['path.package_source'] = str(package_root)
        build['py3pkg.explicit_packages'] = ['hslu.mechanics']
        self.configure_clone_locals(build, self.project.L['./src/pkg_hsmech'])
        return
    pass

class Emulate (conditor.py3pkg.recipe.Emulate) :
    """Emulated hslu-mechanics python package build."""
    NAME = 'hslumechanics-emulate'
    def configure(self, build, flavour) :
        package_root = self.project.path.joinpath('./src/pkg_hsmech').resolve()
        package = conditor.py3pkg.build.Package(build)
        build['path.package_source'] = str(package_root)
        build['py3pkg.explicit_packages'] = ['hslu.mechanics']
        self.configure_clone_locals(build, self.project.L['./src/pkg_hsmech'])
        return
    pass
