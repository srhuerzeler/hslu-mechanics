
import conditor
context = conditor.ctx
def tb_get(index, fallback=None) :
    """Get a build entry from this build with fallback if not existing."""
    if index in context['T'] :
        return context['T'][index]
    return fallback

# -----------------------------------------------------------------------------
# MAIN CONFIGURATION
# -----------------------------------------------------------------------------

project = tb_get('docs.project', 'UNKNOWN PROJECT')
"""The documented project name."""

author = tb_get('docs.author', 'UNKNOWN AUTHOR')
"""Auhor name of the document."""

project_copyright = tb_get('docs.copyright', 'UNKNOWN COPYRIGHT')
"""Copyright statement."""

version = '???VERSION???'
"""Major project version."""

release = '0.0.0'
"""Full project version tag and build."""

# -----------------------------------------------------------------------------
# GENERAL CONFIGURATION
# -----------------------------------------------------------------------------
#source_suffix = {
#    '.rst': 'restructuredtext'
#}
#"""File extensions of source files."""
#source_encoding = 'utf-8-sig'
#"""Encoding of reST files."""

root_doc = 'index'
"""Document name of the root document."""

##exclude_patterns = []
#"""Exclude when looking for source files."""
##include_patterns = []
#"""Pattern to find source files."""
#templates_path = ['_templates']
#"""List of paths, containing templates."""

rst_epilog = ''
"""String that will be included at the end of every source file."""

rst_prolog = ''
"""String that will be included at the beginning of each source file."""

#primary_domain = 'py'
#"""Name of the default domain."""
##manpages_url = ''
#"""URL to cross-reference manpage roles."""


# -----------------------------------------------------------------------------
# HTML OUTPUT CONFIGURATION
# -----------------------------------------------------------------------------

html_theme = 'sphinx_rtd_theme'
"""Default theme used by html builder."""

html_static_path = []
"""Custom static html file paths."""

html_theme_options = {}
"""Default theme options."""

# -----------------------------------------------------------------------------
# EXTENSIONS
# -----------------------------------------------------------------------------

extensions = []
"""Sphinx extensions to load."""

# -----------------------------------------------------------------------------
# EXTENSIONS - Autodoc
# https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html#configuration
# -----------------------------------------------------------------------------
extensions.append('sphinx.ext.autodoc')

autoclass_content = 'class'
autodoc_class_signature = 'separated'
autodoc_member_order= 'bysource'
autodoc_default_options = {
    'members': True,
    'undoc-members': True,
    'show-inheritance': True
}
autodoc_docstring_signature = True
autodoc_mock_imports = []
autodoc_typehints = 'description'
autodoc_typehints_description_target = 'all'
autodoc_type_aliases = {}
autodoc_typehints_format = 'fully-qualified'
autodoc_preserve_defaults = True
autodoc_warningiserror = True
autodoc_inherit_docstrings = True

# -----------------------------------------------------------------------------
# EXTENSIONS - Napoleon
# https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html#configuration
# -----------------------------------------------------------------------------
extensions.append('sphinx.ext.napoleon')

napoleon_google_docstring = True
napoleon_numpy_docstring = False
napoleon_include_init_with_doc = True
napoleon_include_private_with_doc = False
napoleon_include_special_with_doc = False
napoleon_use_admonition_for_examples = False
napoleon_use_admonition_for_notes = False
napoleon_use_admonition_for_references = False
napoleon_use_ivar = False
napoleon_use_param = True
napoleon_use_keyword = True
napoleon_use_rtype = False # Not working ?
napoleon_preprocess_types = True
napoleon_type_aliases = {}
napoleon_attr_annotations = True
napoleon_custom_sections = []


# -----------------------------------------------------------------------------
# EXTENSIONS - MyST Parser
# https://myst-parser.readthedocs.io/en/latest/configuration.html
# -----------------------------------------------------------------------------
extensions.append('myst_parser')

myst_commonmark_only = False
myst_gfm_only = False
myst_enable_extensions = set()
myst_disable_syntax = []
myst_all_links_external = False
#myst_url_schemes = {}
myst_ref_domains = []
myst_number_code_blocks = []
myst_title_to_header = True
myst_heading_anchors = 1
myst_heading_slug_func = None
myst_html_meta = {}
myst_footnote_transition = True
myst_words_per_minute = 200

# -----------------------------------------------------------------------------
# EXTENSIONS - Intersphinx
# https://www.sphinx-doc.org/en/master/usage/extensions/intersphinx.html
# -----------------------------------------------------------------------------
extensions.append('sphinx.ext.intersphinx')

intersphinx_mapping = {
    'python': ('https://docs.python.org/3', None)
}
intersphinx_cache_limit = 5
intersphinx_timeout = None
intersphinx_disabled_reftypes = ['std:doc']

# -----------------------------------------------------------------------------
# EXTENSIONS - TODO
# -----------------------------------------------------------------------------
extensions.append('sphinx.ext.todo')

todo_include_todos = True
todo_emit_warnings = False # TODO: CHANGE ON RELEADE
todo_link_only = False

# -----------------------------------------------------------------------------
# EXTENSIONS - Linkcode
# -----------------------------------------------------------------------------
extensions.append('sphinx.ext.linkcode')

def linkcode_resolve(domain, info) :
    #print('LINKCODE', domain, info)
    if domain != 'py' :
        return None
    if not info['module'] :
        return None
    #view-source:file:///home/codefile_local.py
    return None

# -----------------------------------------------------------------------------
# HTML THEME - RTD Theme
# https://sphinx-rtd-theme.readthedocs.io/en/stable/configuring.html#
# -----------------------------------------------------------------------------
extensions.append('sphinx_rtd_theme')

html_theme = 'sphinx_rtd_theme'
html_theme_options = {
    'collapse_navigation': True,
    'sticky_navigation': True,
    'navigation_depth': 4,
    'includehidden': True,
    'titles_only': False,
    'analytics_id': None,
    'analytics_anonymize_ip': False,
    'display_version': True,
    'logo_only': False,
    'prev_next_buttons_location': 'bottom',
    'style_external_links': False,
    'vcs_pageview_mode': 'blob',
    'style_nav_header_background': '#2980B9'
}





# -----------------------------------------------------------------------------
# SETUP FUNCTION
# -----------------------------------------------------------------------------

def setup(app) :
    #print(app)
    return

